var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var uglify = require('gulp-uglify');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');
var streamify = require('gulp-streamify')

gulp.task('copy-assets', function () {
    return gulp.src([
            './src/styles/app.css',
            './node_modules/normalize-css/normalize.css',
            './src/index.html'
        ])
        .pipe(gulp.dest('./dist/'));
});

gulp.task('prepare-vendor-js', function() {
    return gulp.src([
            './node_modules/es5-shim/es5-shim.min.js',
            './node_modules/es6-shim/es6-shim.min.js'
         ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('prepare-ie9vendor-js', function() {
   return gulp.src([
            './node_modules/html5shiv/dist/html5shiv.min.js',
            './node_modules/json3/lib/json3.min.js'
        ])
       .pipe(concat('ie9vendor.js'))
       .pipe(gulp.dest('./dist/'));
});

gulp.task('build:dev', ['prepare-vendor-js', 'prepare-ie9vendor-js', 'copy-assets'], function () {
    return browserify({
            entries: './src/index.js',
            extensions: ['.js'],
            debug: true
        })
        .transform(babelify, {
            presets: ["es2015"]
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('build', ['prepare-vendor-js', 'prepare-ie9vendor-js', 'copy-assets'], function () {
    return browserify({
            entries: './src/index.js',
            extensions: ['.js']
        })
        .transform(babelify, {
            presets: ["es2015"]
        })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', ['build:dev'], function () {
    gulp.watch('./src/**/*.*', ['build:dev']);
});

gulp.task('default', ['build']);