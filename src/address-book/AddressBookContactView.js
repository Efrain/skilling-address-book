"use strict";

import {EventEmitter2 as EventEmitter} from 'eventemitter2';
import validator from 'validator';

//Apparently there are some incompatibilities with some packages and Babel 6
//I have decided to use require instead of import. But there are some plugins
//to get this fixed
const CountryList = new (require('country-list'));
const jQuery = require('jquery');
const MAX_FIELD_LENGTH = 100;

export class AddressBookContactView extends EventEmitter {
    constructor(model) {
        super();
        
        this._model = model;
        this._mode = AddressBookContactView.VIEW_MODES.NEW;
        this._currentContact = {};
        this._dRootNode = jQuery(AddressBookContactView._HTMLTemplate);
        this._addCountryListOptions();
        this._attachEvents();
    }

    _attachEvents() {
        this._dRootNode.find('.btn-delete').click(event => {
            event.preventDefault();
            this._deleteContact(this._currentContact);
        });
        this._dRootNode.find('.btn-save').click(event => {
            event.preventDefault();
            this._saveContact(this._currentContact);
        });
        this._dRootNode.find('.btn-close').click(event => {
            event.preventDefault();
            this.emit(AddressBookContactView.DONE_EVENT);
        });
    }

    _validateFields() {
        let result = true;

        this._dRootNode.find('input, select').each((index, field) => {
            let fieldValidation = true;

            if(field.name === 'email') {
                fieldValidation = validator.isEmail(field.value);
            } else {
                fieldValidation = field.value.length > 1 && field.value.length < MAX_FIELD_LENGTH;
            }

            result = result && fieldValidation;
            jQuery(field).toggleClass('field-invalid', !fieldValidation);
        });

        return result;
    }

    _saveContact() {
        if(this._validateFields()) {
            this._dRootNode.find('input, select').each((index, field) => this._currentContact[field.name] = field.value);
            if(this._mode === AddressBookContactView.VIEW_MODES.NEW) {
                this._currentContact.id = Date.now();
                this._model.addContact(this._currentContact);
            } else {
                this._model.updateContact(this._currentContact);
            }
            this.emit(AddressBookContactView.DONE_EVENT);
        }
    }

    _deleteContact() {
        this._model.deleteContact(this._currentContact);
        this.emit(AddressBookContactView.DONE_EVENT);
    }

    _addCountryListOptions() {
        let dSelect = this._dRootNode.find('select');

        CountryList.getCodes().forEach(code => {
            dSelect.append(jQuery('<option value="' + code +'">' + CountryList.getName(code)+ '</option>'));
        });
    }
    
    _setInputValues() {
        this._dRootNode.find('input, select').each((index, field) => {
            let value = this._currentContact[field.name];
            field.value = typeof value === 'undefined' ? '': value;
        });
    }

    get rootNode() {
        return this._dRootNode.get(0);
    }
    
    setViewMode(mode, data) {
        this._mode = mode;
        if(mode === AddressBookContactView.VIEW_MODES.NEW) {
            this._currentContact = {};
            this._dRootNode.find('.btn-delete').prop('disabled', true);
        } else {
            this._currentContact = data;
            this._dRootNode.find('.btn-delete').prop('disabled', false);
        }
        this._setInputValues();
    }
    
    static get VIEW_MODES() {
        return {
            NEW:"new",
            EDIT:"edit"
        };
    }

    static get DONE_EVENT() {
        return "done_event";
    }

    static get _HTMLTemplate() {
        return '<form class="address-book-contact-view app-view">' +
                    '<h1 class="view-title">Address Book Contact</h1>' +
                    '<fieldset>' +
                        '<label>First Name:</label>' +
                        '<input name="first-name"/>' +
                        '<label>Last Name:</label>' +
                        '<input name="last-name"/>' +
                        '<label>Email:</label>' +
                        '<input name="email"/>' +
                        '<label>Country Name:</label>' +
                        '<select name="country-code"></select>' +
                    '</fieldset>' +
                    '<section class="buttons">' +
                        '<button class="button btn-delete">Delete</button>' +
                        '<button class="button btn-close">Close</button>' +
                        '<button class="button btn-save">Save</button>' +
                    '</section>' +
                '</form>';
    }
}