"use strict";

import {AddressBookModel as Model} from './AddressBookModel';
import {AddressBookListView as ListView} from './AddressBookListView';
import {AddressBookContactView as ContactView} from './AddressBookContactView';


export class AddressBookController {
    constructor() {
        let contacts = [];
        
        if(localStorage.addressBook) {
            contacts = JSON.parse(localStorage.addressBook);
        }
        
        this._model = new Model(contacts);
        this._listView = new ListView(this._model);
        this._contactView = new ContactView(this._model);
        this._dAppContainer = document.getElementById("app-container");
        
        this._attachEvents();
        this._dAppContainer.appendChild(this._listView.rootNode);
    }
    
    _attachEvents() {
        this._contactView.on(ContactView.DONE_EVENT, () => this._setView(this._listView));
        
        this._listView.on(ListView.NEW_CONTACT_EVENT, () => {
            this._contactView.setViewMode(ContactView.VIEW_MODES.NEW);
            this._setView(this._contactView);
        });
        
        this._listView.on(ListView.EDIT_CONTACT_EVENT, contact => {
            this._contactView.setViewMode(ContactView.VIEW_MODES.EDIT, contact);
            this._setView(this._contactView);
        });
        
        this._model.on(Model.MODEL_UPDATED, () => localStorage.addressBook = JSON.stringify(this._model.contacts));
    }
    
    _setView(view) {
        this._dAppContainer.replaceChild(view.rootNode, this._dAppContainer.firstElementChild);
    }
}