"use strict";

import {EventEmitter2 as EventEmitter} from 'eventemitter2';
import {AddressBookModel as Model} from './AddressBookModel';

//Apparently there are some incompatibilities with some packages and Babel 6
//I have decided to use require instead of import. But there are some plugins
//to get this fixed
const jQuery = require('jquery');
const CountryList = new (require('country-list'));

export class AddressBookListView extends EventEmitter{
    constructor(model) {
        super();
        
        this._model = model;
        this._dRootNode = jQuery(AddressBookListView._HTMLTemplate);
        this._attachEvents();
        this._renderContacts();
    }
    
    _renderContacts() {
        let dList = jQuery(this._dRootNode).find('.address-book-list');
        dList.empty();
        
        this._model.contacts.forEach(contact => {
            let countryName = CountryList.getName(contact['country-code']);
            let dLi = jQuery(AddressBookListView._buildLiTemplate(countryName));
            
            //Prevent rendering markup
            dLi.find('.full-name').text(contact['first-name'] + ' ' + contact['last-name']);
            dLi.find('.email').text(contact.email);
            
            dLi.attr('data-id', contact.id);
            dList.append(dLi);
        });
    }
    
    _attachEvents() {
        this._dRootNode.find('.btn-add').click(() => {
            this.emit(AddressBookListView.NEW_CONTACT_EVENT);
        });
        
        this._dRootNode.find('.btn-edit').click(() => {
            let contactId = parseInt(this._dRootNode.find('.address-book-contact.selected').attr('data-id'));
            this.emit(AddressBookListView.EDIT_CONTACT_EVENT, this._model.getContactById(contactId));
        });
        
        this._dRootNode.find('.address-book-list').delegate('li', 'click', event => {
            this._dRootNode.find('.address-book-contact.selected').removeClass("selected");
            jQuery(event.currentTarget).addClass("selected");
            this._dRootNode.find('.btn-edit').prop('disabled', false);
        });
        this._model.on(Model.MODEL_UPDATED, contacts => this._renderContacts());
    }
    
    get rootNode() {
        return this._dRootNode.get(0);
    }

    static get NEW_CONTACT_EVENT() {
        return "new_contact_event";
    }

    static get EDIT_CONTACT_EVENT() {
        return "edit_contact_event";
    }

    static get _HTMLTemplate() {
        return '<div class="address-book-view app-view">' +
                    '<header>' +
                        '<h1 class="view-title">Address Book</h1>' +
                        '<section class="buttons">' +
                            '<button class="button btn-add">Add Contact</button>' +
                            '<button class="button btn-edit" disabled>Edit Contact</button>' +
                        '</section>' +
                    '</header>' +
                    '<section class="scrollable">' +
                        '<ul class="address-book-list"></ul>' +
                    '</section>' +
            '</div>';
    }

    static _buildLiTemplate(countryName) {
        return '<li class="address-book-contact">' +
                    '<label class="full-name"></label>' +
                    '<label class="email"></label>' +
                    '<label class="country-name">' + countryName + '</label>' +
                '</li>';
    }
}