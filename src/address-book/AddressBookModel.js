"use strict";

import EventEmitter from 'eventemitter2';

export class AddressBookModel extends EventEmitter{
    constructor(contactsData) {
        super();
        
        this._contacts = contactsData.sort(AddressBookModel._sortContacts);
    }
    
    addContact(contactData) {
        this._contacts.push(contactData);
        this._contacts = this._contacts.sort(AddressBookModel._sortContacts);
        this.emit(AddressBookModel.MODEL_UPDATED, this._contacts);
    }
    
    deleteContact(contactData) {
        let index = this._contacts.indexOf(contactData);

        if (index > -1) {
            this._contacts.splice(index, 1);
        }
        this.emit(AddressBookModel.MODEL_UPDATED, this._contacts);
    }
    
    updateContact() {
        this._contacts = this._contacts.sort(AddressBookModel._sortContacts);
        this.emit(AddressBookModel.MODEL_UPDATED, this._contacts);
    }

    getContactById(contactId) {
        return this._contacts[this._contacts.findIndex(({id}) => id === contactId)];
    }
    
    get contacts() {
        return this._contacts;
    }
    
    static get MODEL_UPDATED() {
        return "model_updated";
    }

    static _sortContacts(a, b) {
        let stringA = a['first-name'] + a['last-name'];
        let stringB = b['first-name'] + b['last-name'];

        if (stringA < stringB) return -1;
        if (stringA > stringB ) return 1;
        return 0;
    }
}