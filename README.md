# Address Book

## To build and run the app, execute the following commands:
 * NodeJS and npm will need to be installed in your system
 * 'npm install'
 * 'npm run build'
 * 'npm start'
 * From a browser http://localhost:3000

### I have used Local Storage, but IndexedDB is becoming widely supported by the browsers.

### I have written the exercise using new ES6 syntax and features.

### For the build I have used:
 * Gulp
 * Babel
 * Browserify
 * Uglify
 
### There are some script defined in the package.json
 * 'npm start' Will start a local server for localhost:3000
 * 'npm build' Will aggregate all the dependencies and assets and will create a dist folder.
 * 'npm lint' Will check for code quality. Although I have not customized it or inherited from a known configuration.
 
### There are some dependencies:
 * 'country-list' As part of the requirements
 * 'validator' To validate the email input field
 * 'jQuery' for DOM traversing
 * 'eventemitter2' To support the Observer Pattern
 
 * 'es5-shim' With the polyfills for old browsers.
 * 'normalize-css' To make css styles homogeneous across browsers.
 * 'json3', 'html5shiv' to support IE9
 
### Things to improve:
 * I may add some testing using Karma, Coverage, Jasmine and Sinon. I have been busy the last days but I am going to have
   some hours in the coming day/s.
   
 * The code is not commented, specially parameters types and returns. Otherwise, the functions and variables have
   meaningful names. I will fix that altogether with the tests.
 
 * The dist files do not contain the version or build number. Something that I could also add.
 
 * Use a ESLint configuration widely used like airbnb.